package com.sellics.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sellics.assignment.service.KeywordScoreEstimatorService;

@RestController
public class AmazonScoreEstimateController {
@Autowired
private KeywordScoreEstimatorService keywordScoreEstimatorService;
	
	@GetMapping(path="estimate")
	public Response getKeywordScore(@RequestParam("keyword")String keyWord) {
		Response response=new Response();
		response.setKeyword(keyWord);
		response.setScore(keywordScoreEstimatorService.estimateKeywordScore(keyWord));
		return response;
	}
}

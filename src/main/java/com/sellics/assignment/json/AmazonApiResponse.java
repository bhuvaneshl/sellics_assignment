
package com.sellics.assignment.json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "alias",
    "prefix",
    "suffix",
    "suggestions",
    "responseId",
    "shuffled"
})
public class AmazonApiResponse {

    @JsonProperty("alias")
    private String alias;
    @JsonProperty("prefix")
    private String prefix;
    @JsonProperty("suffix")
    private Object suffix;
    @JsonProperty("suggestions")
    private List<Suggestion> suggestions = null;
    @JsonProperty("responseId")
    private String responseId;
    @JsonProperty("shuffled")
    private Boolean shuffled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("alias")
    public String getAlias() {
        return alias;
    }

    @JsonProperty("alias")
    public void setAlias(String alias) {
        this.alias = alias;
    }

    @JsonProperty("prefix")
    public String getPrefix() {
        return prefix;
    }

    @JsonProperty("prefix")
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @JsonProperty("suffix")
    public Object getSuffix() {
        return suffix;
    }

    @JsonProperty("suffix")
    public void setSuffix(Object suffix) {
        this.suffix = suffix;
    }

    @JsonProperty("suggestions")
    public List<Suggestion> getSuggestions() {
        return suggestions;
    }

    @JsonProperty("suggestions")
    public void setSuggestions(List<Suggestion> suggestions) {
        this.suggestions = suggestions;
    }

    @JsonProperty("responseId")
    public String getResponseId() {
        return responseId;
    }

    @JsonProperty("responseId")
    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    @JsonProperty("shuffled")
    public Boolean getShuffled() {
        return shuffled;
    }

    @JsonProperty("shuffled")
    public void setShuffled(Boolean shuffled) {
        this.shuffled = shuffled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "AmazonApiResponse [alias=" + alias + ", prefix=" + prefix + ", suffix=" + suffix + ", suggestions="
				+ suggestions + ", responseId=" + responseId + ", shuffled=" + shuffled + ", additionalProperties="
				+ additionalProperties + "]";
	}
    
    

}


package com.sellics.assignment.json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "suggType",
    "type",
    "value",
    "refTag",
    "scopes",
    "ghost",
    "help",
    "queryUnderstandingFeatures",
    "xcatOnly",
    "fallback",
    "blackListed",
    "spellCorrected"
})
public class Suggestion {

    @JsonProperty("suggType")
    private String suggType;
    @JsonProperty("type")
    private String type;
    @JsonProperty("value")
    private String value;
    @JsonProperty("refTag")
    private String refTag;
    @JsonProperty("scopes")
    private List<Scope> scopes = null;
    @JsonProperty("ghost")
    private Boolean ghost;
    @JsonProperty("help")
    private Boolean help;
    @JsonProperty("queryUnderstandingFeatures")
    private List<QueryUnderstandingFeature> queryUnderstandingFeatures = null;
    @JsonProperty("xcatOnly")
    private Boolean xcatOnly;
    @JsonProperty("fallback")
    private Boolean fallback;
    @JsonProperty("blackListed")
    private Boolean blackListed;
    @JsonProperty("spellCorrected")
    private Boolean spellCorrected;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("suggType")
    public String getSuggType() {
        return suggType;
    }

    @JsonProperty("suggType")
    public void setSuggType(String suggType) {
        this.suggType = suggType;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @JsonProperty("refTag")
    public String getRefTag() {
        return refTag;
    }

    @JsonProperty("refTag")
    public void setRefTag(String refTag) {
        this.refTag = refTag;
    }

    @JsonProperty("scopes")
    public List<Scope> getScopes() {
        return scopes;
    }

    @JsonProperty("scopes")
    public void setScopes(List<Scope> scopes) {
        this.scopes = scopes;
    }

    @JsonProperty("ghost")
    public Boolean getGhost() {
        return ghost;
    }

    @JsonProperty("ghost")
    public void setGhost(Boolean ghost) {
        this.ghost = ghost;
    }

    @JsonProperty("help")
    public Boolean getHelp() {
        return help;
    }

    @JsonProperty("help")
    public void setHelp(Boolean help) {
        this.help = help;
    }

    @JsonProperty("queryUnderstandingFeatures")
    public List<QueryUnderstandingFeature> getQueryUnderstandingFeatures() {
        return queryUnderstandingFeatures;
    }

    @JsonProperty("queryUnderstandingFeatures")
    public void setQueryUnderstandingFeatures(List<QueryUnderstandingFeature> queryUnderstandingFeatures) {
        this.queryUnderstandingFeatures = queryUnderstandingFeatures;
    }

    @JsonProperty("xcatOnly")
    public Boolean getXcatOnly() {
        return xcatOnly;
    }

    @JsonProperty("xcatOnly")
    public void setXcatOnly(Boolean xcatOnly) {
        this.xcatOnly = xcatOnly;
    }

    @JsonProperty("fallback")
    public Boolean getFallback() {
        return fallback;
    }

    @JsonProperty("fallback")
    public void setFallback(Boolean fallback) {
        this.fallback = fallback;
    }

    @JsonProperty("blackListed")
    public Boolean getBlackListed() {
        return blackListed;
    }

    @JsonProperty("blackListed")
    public void setBlackListed(Boolean blackListed) {
        this.blackListed = blackListed;
    }

    @JsonProperty("spellCorrected")
    public Boolean getSpellCorrected() {
        return spellCorrected;
    }

    @JsonProperty("spellCorrected")
    public void setSpellCorrected(Boolean spellCorrected) {
        this.spellCorrected = spellCorrected;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "Suggestion [suggType=" + suggType + ", type=" + type + ", value=" + value + ", refTag=" + refTag
				+ ", scopes=" + scopes + ", ghost=" + ghost + ", help=" + help + ", queryUnderstandingFeatures="
				+ queryUnderstandingFeatures + ", xcatOnly=" + xcatOnly + ", fallback=" + fallback + ", blackListed="
				+ blackListed + ", spellCorrected=" + spellCorrected + ", additionalProperties=" + additionalProperties
				+ "]";
	}

}

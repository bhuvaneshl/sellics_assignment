package com.sellics.assignment.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sellics.assignment.json.AmazonApiResponse;
import com.sellics.assignment.json.Suggestion;

@Service
public class KeywordEstimatorServiceImpl implements KeywordScoreEstimatorService {

	RestTemplate restTemplate;

	private static final String amazonApiUrl = "https://completion.amazon.com/api/2017/suggestions?page-type=Gateway&lop=en_US&site-variant=desktop&client-info=amazon-search-ui&mid=ATVPDKIKX0DER&alias=aps&b2b=0&fresh=0&ks=65&prefix=%s&limit=15&fb=1&suggestion-type=KEYWORD";

	public KeywordEstimatorServiceImpl() {
		restTemplate = new RestTemplate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sellics.assignment.service.KeywordScoreEstimatorService#
	 * estimateKeywordScore(java.lang.String)
	 */
	@Override
	public int estimateKeywordScore(String keyword) {
		int numberOfSearches = 0;
		int searchScoreSum = 0;
		int curSearchPos = 0;
		// for the given keyword search in amazon by providing all possible sub
		// sequences in the order of the characters in the given keyword
		for (int stringPos = 0; stringPos < keyword.length(); stringPos++) {
			curSearchPos = getExpectedKeyWordPosition(keyword.substring(0, stringPos + 1), keyword);
			// for each response calculate the search score
			// as amazon api gives only 10 suggestions for the given request if position is
			// 1- score is 100, position is 2 - score is 90 and so on upto position is 10-
			// score is 10
			// postion 0 - score is 0 (As no match found in response
			if (curSearchPos != 0) {
				searchScoreSum += (11 - curSearchPos) * 10;
			}
			numberOfSearches++;
			//stop when it reaches postion 1 because all subsequenet searches will return 1
			if(curSearchPos==1) {
				break;
			}
		}
		// dividing sum of the scores with number of searches performed by give me an
		// estimated search-volume score
		return searchScoreSum / numberOfSearches;

	}

	/**
	 * This method searches the amazon api with the given keyword and returns the
	 * position of the expected keyword in suggestions list
	 * 
	 * @param searchKeyword   - search keyword passed in the amazon api
	 * @param expectedKeyword - expected Keyword in amazon Suggestion List
	 * @return position of expectedKeyword <br>
	 *         0- if given keyword not present in the response or <br>
	 *         1-10 based on the first match in the order of the returned
	 *         suggestions List
	 */
	private int getExpectedKeyWordPosition(String searchKeyword, String expectedKeyword) {
		// position zero indicates that keyword not in list
		int position = 0;
		// \b" is called a word boundary. It matches at the start or the end of a word,
		// so it checks whether expected keyword is exactly matched in the given
		// sentence
		String regEx = "\\b" + expectedKeyword + "\\b";
		Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
		AmazonApiResponse apiResponse = restTemplate.getForObject(String.format(amazonApiUrl, searchKeyword),
				AmazonApiResponse.class);
		if (apiResponse != null && apiResponse.getSuggestions() != null) {
			int curPos = 0;
			for (Suggestion suggestion : apiResponse.getSuggestions()) {
				curPos += 1;
				Matcher matcher = pattern.matcher(suggestion.getValue());
				// a match found for the given key word
				if (matcher.find()) {
					position = curPos;
					break;
				}
			}
		}
		return position;
	}

}

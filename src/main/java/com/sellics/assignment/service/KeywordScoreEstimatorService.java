package com.sellics.assignment.service;

public interface KeywordScoreEstimatorService {

	public int estimateKeywordScore(String keyword);
}
